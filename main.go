package main

import (
	"microservices-kwh-meter/routes"
	"microservices-kwh-meter/services"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
)

func init() {
	services.TestConnectionToRabbitMQ()
}

func main() {

	r := gin.Default()
	corsConfig := cors.DefaultConfig()

	corsConfig.AllowOrigins = []string{"*"}
	// To be able to send tokens to the server.
	corsConfig.AllowCredentials = true

	// Register the middleware
	r.Use(cors.New(corsConfig))

	routes.ApiRoute(r) // Parsing to api.go
	r.Run(":8800")     //r.Run(":5000") -> custom ports
}
