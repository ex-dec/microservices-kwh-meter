package services

import (
	"fmt"

	"github.com/streadway/amqp"
)

func TestConnectionToRabbitMQ() (*amqp.Connection, error) {
	conn, err := amqp.Dial("amqp://guest:guest@rabbitmq:5672/")
	if err != nil {
		fmt.Println("--> error connection to rabbitmq: ", err, "\n")
		panic(err)
	}
	defer conn.Close()
	fmt.Println("--> Successfully connected to rabbitMQ \n")
	channel, err := conn.Channel()
	if err != nil {
		fmt.Println("--> error connection to channel rabbitMQ: ", err)
		panic(err)
	}
	defer channel.Close()
	fmt.Println("--> Successfully connected to channel rabbitMQ \n")

	return conn, nil
}
