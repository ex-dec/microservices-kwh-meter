package version

import (
	"microservices-kwh-meter/controllers"

	"github.com/gin-gonic/gin"
)

func APIversionV0(route *gin.Engine) {
	v0 := route.Group("/api/v0/")
	v0.GET("test", controllers.RootHandler)

	v0.POST("example-data-user", controllers.SendDataUser)
}
